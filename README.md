##Pitt
[pitt] Pitt is a stack based language like 'Forth'. [I know there are many]

##Features
[sim] Pitt has a simulator mode [interpreter that could simulate the program without compiling] [Under-Work]

[com] Pitt has a compiler mode [Coming Soon]

##Modes
[RPN] Pitt uses RPN or Reverse-Polish-Notation for expression[expr] solving
and others like 'if' statements

([Pitt is not gonna support our normal expr like '2+3*4'])

##Bugs
Report bugs at discord [I/O Mixture#5239]


##Thanks for checking it out
