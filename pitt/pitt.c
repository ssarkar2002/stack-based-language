#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define bool int
#define true 1
#define false 0

int isNumber(char *string)
{
  int __expect = strlen(string);
  int __found = 0;
  for(int i = 0; i < (int)strlen(string)+1; i++){
    if(string[i] >= '0' && string[i] <= '9'){
      __found  += 1;
    }
  }
  if(__found == __expect){
    return true;
  }
  else{
    return false;
  }
}

// Operator
#define OP_PLUS 0
#define OP_MINUS 1
#define OP_MUL 2
#define OP_DIV 3
#define OP_DUMP 4
#define OP_EQUAL 5
#define OP_DUP 6
#define OP_GT 7
#define OP_LT 8
#define OP_IF 9
#define OP_ELSE 10
#define OP_END 11
#define OP_WRITE 12
#define OP_PUSHABLE 13
int OP_COUNT = OP_PUSHABLE + 1;

const char typePlus = '+';
const char typeMinus = '-';
const char typeMul = '*';
const char typeDiv = '/';
const char typeDump = '.';
const char typeEqual = '=';
const char typeGt = '>';
const char typeLt = '<';

const char* dup = "dup";
const char* write = "write";
const char* block_if = "if";
const char* block_end = "end";
const char* block_else = "else";

// ------ Pair ------
struct ipair{
    int opType;
    int val;
};

// TODO: fix find_end_block on some points e.g.=>
int find_end_block(struct ipair *pair,int loc,int len)
{
  int end_block = -1;
  /*
    2 2 + 4 = if
      1 = if <- getting breaked at this point
        1.
      end
    end
   */
  for(int i = loc+1; i <= len; i++){
    if(pair[i].opType == OP_ELSE){
      break;
    }
    else if(pair[i].opType == OP_IF){
      break;
    }
    
    if(pair[i].opType == OP_END){
      end_block += i;
      break;
    }
  }
  return end_block;
}

int get_size_of_file(const char* path)
{
  FILE *program = fopen(path,"r");

  if(!program){
    printf("Error: Expected a valid program-file-path\n");
    printf("FILE:\n");
    printf("    Can't open    '%s'\n",path);    
  }
  
  fseek(program,0,SEEK_END);
  int size = ftell(program)+1;
  fseek(program,0,SEEK_SET);

  return size;
}

struct ipair *load_program_from_file(const char* path)
{
    FILE *program = fopen(path,"r");

    if(!program){
      printf("Error: Expected a valid program-file-path\n");
      printf("FILE:\n");
      printf("    Can't open    '%s'\n",path);
    }

    fseek(program,0,SEEK_END);
    int size = ftell(program);
    fseek(program,0,SEEK_SET);

    if(size < 1){
      exit(0);
    }

    struct ipair *pitt_pairs = (struct ipair*)malloc(sizeof(struct ipair)*size+1);
    // ---- Parsing Words & tokens ----
    char tm[size+1];
    int pair_trace = 0;

    while(fgets(tm,size+1,program) != NULL){
      char *pitt_words = strtok(tm," ");
      while(pitt_words != NULL){
	
	if(isNumber(pitt_words) == 1){
	  pitt_pairs[pair_trace].opType = OP_PUSHABLE;
	  pitt_pairs[pair_trace].val = atoi(pitt_words);
	  pair_trace += 1;
	}
	else if(strcmp(pitt_words,dup)==0){
	  pitt_pairs[pair_trace].opType = OP_DUP;
	  pitt_pairs[pair_trace].val = 0;
	  pair_trace += 1;
	}
	
        else if(strcmp(pitt_words,block_if)==0){
	  pitt_pairs[pair_trace].opType = OP_IF;
	  pitt_pairs[pair_trace].val = 0;
	  pair_trace += 1;
	}
	else if(strcmp(pitt_words,block_else)==0){
	  pitt_pairs[pair_trace].opType = OP_ELSE;
	  pitt_pairs[pair_trace].val = 0;
	  pair_trace += 1;
	}
	else if(strcmp(pitt_words,block_end)==0){
	  pitt_pairs[pair_trace].opType = OP_END;
	  pitt_pairs[pair_trace].val = 0;
	  pair_trace += 1;
	}

	else if(strcmp(pitt_words,write)==0){
	  pitt_pairs[pair_trace].opType = OP_WRITE;
	  pitt_pairs[pair_trace].val = 0;
	  pair_trace += 1;
	}
	
	
	// Checking Sign
	for(int i = 0; i < strlen(pitt_words)+1; i++){
	  if(pitt_words[i] == typePlus){
	    pitt_pairs[pair_trace].opType = OP_PLUS;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }

	  else if(pitt_words[i] == typeLt){
	    pitt_pairs[pair_trace].opType = OP_LT;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }

	  else if(pitt_words[i] == typeMinus){
	    pitt_pairs[pair_trace].opType = OP_MINUS;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }

	  else if(pitt_words[i] == typeDiv){
	    pitt_pairs[pair_trace].opType = OP_DIV;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }

	  else if(pitt_words[i] == typeMul){
	    pitt_pairs[pair_trace].opType = OP_MUL;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }

	  else if(pitt_words[i] == typeDump){
	    pitt_pairs[pair_trace].opType = OP_DUMP;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }
	  else if(pitt_words[i] == typeEqual){
	    pitt_pairs[pair_trace].opType = OP_EQUAL;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }
	  else if(pitt_words[i] == typeGt){
	    pitt_pairs[pair_trace].opType = OP_GT;
	    pitt_pairs[pair_trace].val = 0;
	    pair_trace += 1;
	  }
	}

	pitt_words = strtok(NULL," ");
      }
    }
    
    return pitt_pairs;
}

// ------ Usage ------
void usage()
{
  printf("Usage: pitt <SUBCOMMAND> [ARGS]\n");
  printf("SUBCOMMANDS:\n");
  printf("     sim     Simulate the program\n");
  printf("     com     Compile the program\n");
  exit(0);
}


// ------ Simulate Program ------
void simulate(struct ipair *program,int program_counter)
{
  int stack[program_counter];
  int stack_push = -1;

  for(int i = 0; i < program_counter; i++){
    if(program[i].opType == OP_PUSHABLE){
      stack_push += 1;
      stack[stack_push] = program[i].val;
    }

    else if(program[i].opType == OP_PLUS){
      int a = stack[stack_push];
      stack_push -= 1;
      int b = stack[stack_push];
      stack_push -= 1;
      
      stack_push += 1;
      stack[stack_push] = a + b;
      //printf("%d\n",stack[stack_push]);
      
    }

    else if(program[i].opType == OP_DUP){
      int a = stack[stack_push];
      stack_push += 1;
      stack[stack_push] = a;
      //printf("%d\n",a);
    }

    else if(program[i].opType == OP_MINUS){
      int a = stack[stack_push];
      stack_push -= 1;
      int b = stack[stack_push];
      stack_push -= 1;
      
      stack_push += 1;
      stack[stack_push] = b - a;
      //printf("%d\n",stack[stack_push]);      
    }

    else if(program[i].opType == OP_MUL){
      int a = stack[stack_push];
      stack_push -= 1;
      int b = stack[stack_push];
      stack_push -= 1;
      
      stack_push += 1;
      stack[stack_push] = a * b;
      //printf("%d\n",stack[stack_push]);
      
    }

    else if(program[i].opType == OP_DIV){
      int a = stack[stack_push];
      stack_push -= 1;
      int b = stack[stack_push];
      stack_push -= 1;
      
      stack_push += 1;
      stack[stack_push] = b / a;
      //printf("%d\n",stack[stack_push]);     
    }
 
    else if(program[i].opType == OP_EQUAL){
      int a = stack[stack_push];
      stack_push -= 1;
      int b = stack[stack_push];
      stack_push -= 1;

      if(a == b){
        stack_push += 1;
        stack[stack_push] = true;
        //printf("%d\n",stack[stack_push]);
      }
      else{
	stack_push += 1;
	stack[stack_push] = false;
	//printf("%d\n",stack[stack_push]);
      }
    }

    else if(program[i].opType == OP_GT){
      int a = stack[stack_push];
      stack_push -= 1;
      int b = stack[stack_push];
      stack_push -= 1;

      if(b > a){
	stack_push += 1;
	stack[stack_push] = true;
      }
      else{
	stack_push += 1;
	stack[stack_push] = false;
      }
    }
    else if(program[i].opType == OP_LT){
      int a = stack[stack_push];
      stack_push -= 1;
      int b = stack[stack_push];
      stack_push -= 1;

      if(b < a){
	stack_push += 1;
	stack[stack_push] = true;
      }
      else{
	stack_push += 1;
	stack[stack_push] = false;
      }
    }

    else if(program[i].opType == OP_WRITE){
      int a = stack[stack_push];
      printf("%d\n",a);
    }
    
    // -- TODO: 'else' block --
    else if(program[i].opType == OP_IF){
      int a = stack[stack_push];
           
      if(a == true){

      }
      else if(a == false){
	int end_block = find_end_block(program,i,program_counter);
	if(end_block > -1){
	  i = end_block;
	}
	else if(end_block == -1){
	  printf("'if' instruction does not have a reference to the end of its block\nuse 'end' to reference 'if' to the end.\n");
	}
      }
      
    }
    // -- TODO: if block --
    
  }
  
}

// -- TODO: Compiling Program --
void compile(struct ipair *program,char *output,int program_size)
{
  FILE *fp = fopen("output.asm","w");
  fprintf(fp,"segment .test\n");
  fprintf(fp,"global _start\n");
  fprintf(fp,"_start:\n");

  // -- Compiling Program --
  for(int i = 0; i < program_size; i++){
    if(program[i].opType == OP_PUSHABLE){
      fprintf(fp,"     ;; -- push %d --\n",program[i].val);
      fprintf(fp,"     push %d\n",program[i].val);
    }
    else if(program[i].opType == OP_PLUS){
      fprintf(fp,"     ;; -- plus --\n");
      fprintf(fp,"     pop rax\n");
      fprintf(fp,"     pop rbx\n");
      fprintf(fp,"     add rax,rbx\n");
      fprintf(fp,"     push rax\n");
    }
    else if(program[i].opType == OP_MINUS){
      fprintf(fp,"     ;; -- minus --\n");
      fprintf(fp,"     pop rax\n");
      fprintf(fp,"     pop rbx\n");
      fprintf(fp,"     sub rax, rbx\n");
      fprintf(fp,"     push rax\n");
    }
    else if(program[i].opType == OP_DUMP){
      fprintf(fp,"     ;; -- Dump Unimplement --\n");
      fprintf(fp,"     ;; TODO: Dump\n");
    }
  }

  fprintf(fp,"     mov rax, 60\n");
  fprintf(fp,"     mov rdi, 0\n");
  fprintf(fp,"     syscall\n");
  
  system("nasm -felf64 output.asm -o output.o");
  
  char buf[32];
  sprintf(buf,"ld output.o -o %s",output);


  system(buf);
  
  fclose(fp);
  remove("output.asm");
  remove("output.o");
}
// -- Compiler Work Paused --

int main(int argc,char **argv)
{
  if(argc < 2){
    usage();
    exit(0);
  }

  char *mode = argv[1];
  char *file = argv[2];

  struct ipair *_program_file = load_program_from_file(file);
  int size = get_size_of_file(file);
  
  if(strcmp(mode , "sim")==0){
    simulate(_program_file,size);
  }
  else if(strcmp(mode , "com")==0){
    char *output_file = argv[3];
    compile(_program_file,output_file,size);
  }
  else{
    printf("Unknown program mode '%s'\n",mode);
    exit(0);
  }

  return 0;
}
